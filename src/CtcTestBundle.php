<?php


namespace CtcMediaTest\CtcTestBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CtcTestBundle
 *
 * @package CtcMediaTest\CtcTestBundle
 */
class CtcTestBundle extends Bundle
{
}