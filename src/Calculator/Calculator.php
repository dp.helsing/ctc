<?php

namespace CtcMediaTest\CtcTestBundle\Calculator;

use MathParser\StdMathParser;
use MathParser\Interpreting\Evaluator;

/**
 * Class Calculator
 *
 * @package CtcMediaTest\CtcTestBundle\Calculator
 */
class Calculator implements CalculatorInterface
{
    /**
     * Расчет данных
     *
     * @param string $input входные данные
     *
     * @return mixed
     */
    public function calculate(string $input)
    {
        $parser = new StdMathParser();

        $AST = $parser->parse($input);

        $evaluator = new Evaluator();

        $value = $AST->accept($evaluator);

        return $value;
    }
}