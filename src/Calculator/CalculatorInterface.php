<?php

namespace CtcMediaTest\CtcTestBundle\Calculator;

/**
 * Interface CalculatorInterface
 *
 * @package CtcMediaTest\CtcTestBundle\Calculator
 */
interface CalculatorInterface
{
    /**
     * Расчет данных
     *
     * @param string $input входные данные для расчета
     *
     * @return mixed
     */
    public function calculate(string $input);
}