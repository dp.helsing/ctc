<?php

namespace CtcMediaTest\CtcTestBundle\Command;

use CtcMediaTest\CtcTestBundle\Calculator\CalculatorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

/**
 * Class CalculateCommand
 *
 * @package CtcMediaTest\CtcTestBundle\Command
 */
class CalculateCommand extends Command
{
    /**
     * string
     */
    public const COMMAND_NAME = 'ctc-media:calculator';

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var CalculatorInterface
     */
    private $calculator;

    /**
     * CalculateCommand constructor.
     *
     * @param CalculatorInterface $calculator
     */
    public function __construct(CalculatorInterface $calculator)
    {
        parent::__construct(self::COMMAND_NAME);

        $this->calculator = $calculator;
    }

    /**
     * @return void
     */
    protected function configure()
    {
        parent::configure();

        $this->setDescription('Try calculate');
        $this->addArgument('value', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);

        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io->title('Start calculate');
        try {
            $this->io->success(
                sprintf(
                    'Result is "%s"',
                    $this->calculator->calculate(
                        $input->getArgument('value')
                    )
                )
            );
        } catch (Throwable $exception) {
            $this->io->error('Exception occurred: ' . $exception->getMessage());
            $this->io->text($exception->getTraceAsString());
        }
    }
}