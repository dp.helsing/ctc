<?php

namespace CtcMediaTest\CtcTestBundle\DependencyInjection;

use CtcMediaTest\CtcTestBundle\Calculator\Calculator;
use CtcMediaTest\CtcTestBundle\Command\CalculateCommand;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class HealthCheckExtension
 *
 * @package CtcMediaTest\CtcTestBundle\DependencyInjection
 */
class CtcTestExtension extends Extension
{
    /**
     * @param array            $configs
     * @param ContainerBuilder $container
     *
     * @return void
     *
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $commandDefinition = new Definition(CalculateCommand::class);
        $commandDefinition->addArgument(new Reference(Calculator::class));
        $commandDefinition->addTag('console.command', ['command' => CalculateCommand::COMMAND_NAME]);
        $container->setDefinition(CalculateCommand::class, $commandDefinition);
    }
}