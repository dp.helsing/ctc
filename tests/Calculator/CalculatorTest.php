<?php

namespace CtcMediaTest\CtcTestBundle\Tests\Calculator;

use CtcMediaTest\CtcTestBundle\Calculator\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    /**
     * @return array
     */
    public function getData()
    {
        return [
            [
                '1+1',
                2
            ],
            [
                '2*2',
                4
            ],
        ];
    }

    /**
     * @dataProvider getData
     *
     * @param string $input  calculate value
     * @param mixed  $result result value
     *
     * @return void
     */
    public function testCalculate($input, $result)
    {
        $c = new Calculator();

        $this->assertEquals(
            $c->calculate($input),
            $result
        );
    }
}